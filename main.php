<?php

use G3GildedRose\GildedRose;
use G3GildedRose\Item\Item;

require_once './vendor/autoload.php';


$items = array();

$items [] = new Item("+5 Dexterity Vest", 10, 20);
$items [] = new Item("Aged Brie", 2, 0);
$items [] = new Item("Elixir of the Mongoose", 5, 7);
$items [] = new Item("Sulfuras, Hand of Ragnaros", 0, 80);
$items [] = new Item("Backstage passes to a TAFKAL80ETC concert", 15, 20);
$items [] = new Item("Conjured Mana Cake", 3, 6);

$gildedRose = new GildedRose();
$updatedItems = $gildedRose->updateQuality($items);

foreach ($updatedItems as $item) {
    echo "Item: {$item->getName()}, Quality: {$item->getQuality()}, SellIn: {$item->getSellIn()}\n";
}
